package com.example.mysyncadapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import com.example.sqlite.DataSourceManager;
import com.example.sqlite.Utils;

public class MySyncAdaptor extends AbstractThreadedSyncAdapter{

	private Context mContext;
	private String TAG = MySyncAdaptor.class.getName();
	private DataSourceManager dataManager;
	public static final String[] listedFolders = {"css", "html5", "images", "js"};
	
	public MySyncAdaptor(Context context, boolean autoInitialize) {
		super(context, autoInitialize);
		mContext = context;
		dataManager = new DataSourceManager(mContext);
	}
	
	@SuppressLint("SimpleDateFormat")
	@Override
	public void onPerformSync(Account account, Bundle extras, String authority,	ContentProviderClient provider, SyncResult syncResult) {
		android.os.Debug.waitForDebugger();
		FtpFileTransferClient client = new FtpFileTransferClient("electricityservice.hostei.com", "a8926151", "ishaan007", null);
		
		String lastUpdate = dataManager.getLastSynDate();
		List<FileMetaData> filesOnDisk = dataManager.getEntries("");
		if(filesOnDisk.size() <= 0){
			if(client.connect())
				client.downloadAll();
			else{
				Log.i(TAG, "FTP connection failed, returning..");
				return;
			}
		}else{
			//initially get the files which are missing on local
			// and which are deleted from remote..
			List<FileMetaData> missingFiles = new ArrayList<FileMetaData>();
			
			List<FileMetaData> filesOnRemote = client.getList();
			
			//missing files on device..
			for(FileMetaData remoteFile: filesOnRemote){
				String nameOfRemoteFile = remoteFile.getFileName();
				
				boolean isPresent = false;
				//check if this file is present on device..
				for(FileMetaData localFile: filesOnDisk){
					String nameOfLocalFile = localFile.getFileName();
					
					if(nameOfLocalFile.equals(nameOfRemoteFile)){
						isPresent = true;
						break;
					}
				}
				
				//if not present add to downloading files..
				if(!isPresent)
					missingFiles.add(remoteFile);
			}
			
			//download if missing files are found..
			if(missingFiles.size() > 0)
				client.addToDownload(missingFiles);
/*____________________________________________________________________________________________________________________*/			
		
			//finally get updated files..
			List<FileMetaData> filesUpdated = new ArrayList<FileMetaData>();
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
			Date dateOfSync = null;
			try {
				dateOfSync = dateFormat.parse(lastUpdate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
			
			//find the updated files after this date
			for(FileMetaData remoteFile: filesOnRemote){
				String modifiedDate = remoteFile.getTimestamp();
				Date remoteModified = null;
				try {
					remoteModified = dateFormat.parse(modifiedDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				if(remoteModified != null && dateOfSync != null){
					if(remoteModified.after(dateOfSync))
						filesUpdated.add(remoteFile);
				}
			}
			//add to download list if file is updated..
			if(filesUpdated.size() > 0)
				client.addToDownload(filesUpdated);
			
/*____________________________________________________________________________________________________________________*/
			
			List<FileMetaData> filesToBeDeleted = new ArrayList<FileMetaData>();
			
			//find files which are deleted from remote and update local..
			for(FileMetaData localFile: filesOnDisk){
				String nameOfLocalFile = localFile.getFileName();
				
				boolean isDeleted = true;
				//check if the file is present on remote..
				for(FileMetaData remoteFile: filesOnRemote){
					String nameOfRemoteFile = remoteFile.getFileName();
					
					if(nameOfLocalFile.equals(nameOfRemoteFile)){
						isDeleted = false;
						break;
					}
				}
				//if deleted add to delete files list..
				if(isDeleted)
					filesToBeDeleted.add(localFile);
			}
			
			//delete the files if any..
			if(filesToBeDeleted.size() > 0)
				new Utils().deleteFiles(filesToBeDeleted);
			
			client.disconnect();
		}
	}
}