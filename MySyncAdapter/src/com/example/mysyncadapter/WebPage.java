package com.example.mysyncadapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class WebPage extends Activity {

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_page);
		
		Intent myIntent = getIntent();
		Bundle bundle = myIntent.getExtras();
		String url = bundle.getString("webpage");
		
		WebView web = (WebView) findViewById(R.id.web);
		WebSettings settings = web.getSettings();
		settings.setJavaScriptEnabled(true);
		web.loadUrl("file://" + url);
	}	
}
