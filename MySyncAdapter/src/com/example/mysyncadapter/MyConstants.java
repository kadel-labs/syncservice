package com.example.mysyncadapter;

import android.net.Uri;

public class MyConstants
{
	public static final String AUTHORITY = "com.example.mysyncadapter.fileMeta.provider";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/file_meta_data");
}
