package com.example.mysyncadapter;

import static com.example.mysyncadapter.MyConstants.AUTHORITY;

import com.example.sqlite.MyDbHelper;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class MyContentProvider extends ContentProvider
{

    public static final UriMatcher URI_MATCHER = buildUriMatcher();
    public static final String PATH = "file_meta_data";
    public static final int PATH_TOKEN = 100;

    // Uri Matcher for the content provider
    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = AUTHORITY;
        matcher.addURI(authority, PATH, PATH_TOKEN);
        return matcher;
    }

    // Content Provider stuff

    private MyDbHelper dbHelper;

    @Override
    public boolean onCreate()
    {
        Context ctx = getContext();
        dbHelper = new MyDbHelper(ctx);
        return true;
    }

    @Override
    public String getType(Uri uri)
    {
        final int match = URI_MATCHER.match(uri);
        switch (match)
        {
            case PATH_TOKEN:
                return "vnd.android.cursor.dir/vnd." + AUTHORITY + ".file_meta_data";
            default:
                throw new UnsupportedOperationException("URI " + uri + " is not supported.");
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        final int match = URI_MATCHER.match(uri);
        switch (match)
        {
            // retrieve file list
            case PATH_TOKEN:
            {
                SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
                builder.setTables(MyDbHelper.SYNC_DATA_TABLE_NAME);
                return builder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
            }
            default:
                return null;
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int token = URI_MATCHER.match(uri);
        switch (token)
        {
            case PATH_TOKEN:
            {
                long id = db.insert(MyDbHelper.SYNC_DATA_TABLE_NAME, null, values);
                if (id != -1)
                    getContext().getContentResolver().notifyChange(uri, null);
                return MyConstants.CONTENT_URI.buildUpon().appendPath(String.valueOf(id)).build();
            }
            default:
            {
                throw new UnsupportedOperationException("URI: " + uri + " not supported.");
            }
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int token = URI_MATCHER.match(uri);
        int rowsDeleted = -1;
        switch (token)
        {
            case (PATH_TOKEN):
                rowsDeleted = db.delete(MyDbHelper.SYNC_DATA_TABLE_NAME, selection, selectionArgs);
                break;
//            case (PATH_FOR_ID_TOKEN):
//                String tvShowIdWhereClause = MyDbHelper.SYNC_DATA_COL_ID + "=" + uri.getLastPathSegment();
//                if (!TextUtils.isEmpty(selection))
//                    tvShowIdWhereClause += " AND " + selection;
//                rowsDeleted = db.delete(MyDbHelper.SYNC_DATA_TABLE_NAME, tvShowIdWhereClause, selectionArgs);
//                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        // Notifying the changes, if there are any
        if (rowsDeleted != -1)
            getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    /**
     * Man..I'm tired..
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }
}