package com.example.mysyncadapter;

import java.io.Serializable;

public class FileMetaData implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String fileName;
	private String key;
	private String extKey;
	private long fileSize;
	private String timestamp;

	public FileMetaData(){
		super();
	}
	
	public String getFileName() {
		return fileName;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		FileMetaData file = (FileMetaData) o;

		if (timestamp != file.timestamp) return false;
		if (!fileName.equals(file.fileName)) return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = fileName.hashCode() + timestamp.hashCode();
		result = 31 * result;
		return result;
	}

	@Override
	public String toString()
	{
		return fileName;// + " (" + timestamp + ")";
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getExtKey() {
		return extKey;
	}

	public void setExtKey(String extKey) {
		this.extKey = extKey;
	}
}