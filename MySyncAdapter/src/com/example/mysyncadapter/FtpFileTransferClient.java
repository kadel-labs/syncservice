package com.example.mysyncadapter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import android.util.Log;

import com.example.sqlite.Utils;

public class FtpFileTransferClient
{
    private String hostname;
    private String username;
    private String password;
    private Integer port;
    private String TAG = FtpFileTransferClient.class.getName();
    private FTPClient ftpClient;

    public FtpFileTransferClient(String hostname, String username, String password, Integer port)
    {
        this.hostname = hostname;
        this.username = username;
        this.password = password;
        this.port = port;
    }

    public boolean connect()
    {
        try
        {
            Log.i(TAG, "ftp connect to '" + username + "@" + hostname + "'");
            ftpClient = new FTPClient();

			// connect to ftp server
            if(port == null)
            	ftpClient.connect(InetAddress.getByName(hostname));
            else
            	ftpClient.connect(InetAddress.getByName(hostname), port);
            
            
            if(!ftpClient.login(username, password))
            {
                Log.e(TAG, "ftp login failed for'" + username + "@" + hostname + "'");
                return false;
            }

			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			ftpClient.setConnectTimeout(1000 * 1000);
        }
        catch (IOException e)
        {
            Log.e(TAG, "ftp connect failed", e);
            return false;
        }
        return true;
    }
    
    public boolean disconnect()
    {
        Log.i(TAG, "ftp disconnect from '" + username + "@" + hostname + "'");
        try
        {
            // disconnect from ftp server
            if(!ftpClient.logout())
            {
                Log.e(TAG, "ftp logout failed");
            }
            ftpClient.disconnect();
            return true;
        }
        catch (IOException e)
        {
            Log.e(TAG, "ftp disconnect failed", e);
            return false;
        }
    }

    private boolean storeLocally(String remotePath, String download, FileMetaData fileToDownload){
    	File downloadDirectory = new File(download);
    	ftpClient.enterLocalPassiveMode();
    	if(!downloadDirectory.exists()){
    		downloadDirectory.mkdirs();
    	}
    	try {
    		FileOutputStream outputFile = new FileOutputStream(downloadDirectory + "/" + fileToDownload.getFileName());
			ftpClient.retrieveFile(remotePath, outputFile);
			Log.i(TAG, "file download from server to " + downloadDirectory + "successful");
			return true;
		} catch (IOException e) {
			Log.e(TAG, "file download from server to " + downloadDirectory + "failed");
			e.printStackTrace();
			return false;
		}
    }
    
    public void addToDownload(List<FileMetaData> filesToDownload){
    	File newDirectory = new File(MySyncService.FILE_DIRECTORY + "/bengaluru");
		if(!newDirectory.exists()){
			newDirectory.mkdirs();
		}
    	
    	for(int i = 0; i < filesToDownload.size(); i++){
			String directory = filesToDownload.get(i).getKey();
			String extension = filesToDownload.get(i).getExtKey();
			boolean isDownloaded;
			if(extension != "")
				isDownloaded = storeLocally("public_html/bengaluru/" + directory + "/" + extension + "/" + filesToDownload.get(i).getFileName(), (newDirectory + "/" + directory + "/" + extension), filesToDownload.get(i));
			else
				isDownloaded = storeLocally("public_html/bengaluru/" + directory + "/" + filesToDownload.get(i).getFileName(), (newDirectory + "/" + directory + "/"), filesToDownload.get(i));
			
			if(isDownloaded)
				new Utils().addToDatabase(directory, filesToDownload.get(i));
			else
				Log.e(TAG, "Download failed, not storing in db");
		}
    }
    
	public void downloadAll() {
		List<FileMetaData> filesToDownload = getList();
		
		File newDirectory = new File(MySyncService.FILE_DIRECTORY + "/bengaluru");
		
		if(!newDirectory.exists()){
			newDirectory.mkdirs();
		}
		
		for(int i = 0; i < filesToDownload.size(); i++){
			String directory = filesToDownload.get(i).getKey();
			String extension = filesToDownload.get(i).getExtKey();
			
			boolean isDownloaded;
			if(extension != "")
				isDownloaded = storeLocally("public_html/bengaluru/" + directory + "/" + extension + "/" + filesToDownload.get(i).getFileName(), (newDirectory + "/" + directory + "/" + extension), filesToDownload.get(i));
			else
				isDownloaded = storeLocally("public_html/bengaluru/" + directory + "/" + filesToDownload.get(i).getFileName(), (newDirectory + "/" + directory + "/"), filesToDownload.get(i));
			
			if(isDownloaded)
				new Utils().addToDatabase(directory, filesToDownload.get(i));
			else
				Log.e(TAG, "Download failed, not storing in db");
		}
	}
	
	public synchronized List<FileMetaData> getList(){
		if(ftpClient == null){
			
			if(!connect()){
				Log.e(TAG, "Error in connection");
				return null;
			}
		}
		
		String baseUrl = "public_html/bengaluru/";
		
		List<FileMetaData> files = new ArrayList<FileMetaData>();
		
		ftpClient.enterLocalPassiveMode();
		FTPFile[] folders = null;
		try {
			folders = ftpClient.listFiles(baseUrl);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		for(int i = 0; i < folders.length; i++){	
			
			if(folders[i].getName().equals(".") || folders[i].getName().equals(".."))
				continue;
			
			try {
				FTPFile[] remoteFiles = ftpClient.listFiles(baseUrl + folders[i].getName());

				for(FTPFile remoteFile: remoteFiles){

					if(remoteFile.getName().equals(".") || remoteFile.getName().equals(".."))
						continue;	//break for this loop count if it is not a file or directory

					if(remoteFile.isDirectory() && (!remoteFile.getName().equals(".") || !remoteFile.getName().equals(".."))){
						FTPFile[] remoteDirectoryFiles = ftpClient.listFiles(baseUrl + folders[i].getName() + "/" + remoteFile.getName());
						
						for(FTPFile directoryFile: remoteDirectoryFiles){
							if(directoryFile.getName().equals(".") || directoryFile.getName().equals(".."))
								continue;
							FileMetaData file = getFilesInDirectory(baseUrl + folders[i].getName() + "/", directoryFile, folders[i].getName(), remoteFile.getName(), true);
							files.add(file);
						}
					} else {
						FileMetaData file = getFilesInDirectory(baseUrl, remoteFile, folders[i].getName(), "", false);
						files.add(file);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		return files;
	}
	
	private FileMetaData getFilesInDirectory(String baseUrl, FTPFile remoteFile, String key, String extension, boolean flag) {
		
		FileMetaData file = new FileMetaData();
		file.setFileName(remoteFile.getName());
		file.setKey(key);
		file.setExtKey(extension);
		file.setFileSize(remoteFile.getSize());
		String dateOfModification = "";
		try {
			if(ftpClient != null){
				String path = key;
				if(flag)
					path = extension;
				String absolutePath = baseUrl + path + "/" + remoteFile.getName();
				dateOfModification = ftpClient.getModificationTime(absolutePath);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		file.setTimestamp(dateOfModification.split(" ")[1]);

		return file;
	}
}
