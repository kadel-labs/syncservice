package com.example.mysyncadapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sqlite.DataSourceManager;

public class MainActivity extends Activity
{
	// The authority for the sync adapter's content provider
    public static final String AUTHORITY = "com.example.mysyncadapter.fileMeta.provider";
    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "example.com";
    // The account name
    public static final String ACCOUNT = "FlipbookAccount";
    // Instance fields
    Account mAccount;
    LinearLayout fileList;
 // Sync interval constants
    public static final long MILLISECONDS_PER_SECOND = 1000L;
    public static final long SECONDS_PER_MINUTE = 60L;
    public static final long SYNC_INTERVAL_IN_MINUTES = 1L;
    public static final long SYNC_INTERVAL = SYNC_INTERVAL_IN_MINUTES * SECONDS_PER_MINUTE * MILLISECONDS_PER_SECOND;
    private DataSourceManager dataManager;
    // Global variables
    // A content resolver for accessing the provider
    ContentResolver mResolver;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Create the dummy account
        mAccount = createSyncAccount(this);
        /*
         * Turn on periodic syncing
         */
        ContentResolver.addPeriodicSync(mAccount, AUTHORITY, new Bundle(), SYNC_INTERVAL);
        ContentResolver.setSyncAutomatically(mAccount, AUTHORITY, true);
        
        //Get Scroll view
        fileList = (LinearLayout) findViewById(R.id.fileList);
        
        // Get button views
        ImageButton refreshBtn = (ImageButton) findViewById(R.id.refreshBtn);
        Button lclFilesBtn = (Button) findViewById(R.id.lclFilesBtn);
        Button remoteFilesBtn = (Button) findViewById(R.id.remoteFilesBtn);
        
        // Add button listeners
        refreshBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v)
			{
				// Pass the settings flags by inserting them in a bundle
		        Bundle settingsBundle = new Bundle();
		        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
		        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
		        /*
		         * Request the sync for the default account, authority, and
		         * manual sync settings
		         */
		        ContentResolver.requestSync(mAccount, AUTHORITY, settingsBundle);
			}
		});
        
        lclFilesBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v)
			{
//				ContentProviderClient provider = mResolver.acquireContentProviderClient(AUTHORITY);
				List<FileMetaData> localFiles = new ArrayList<FileMetaData>();
				
				dataManager = new DataSourceManager();
				localFiles = dataManager.getEntries("");
				if(localFiles == null){
					dataManager = new DataSourceManager(getApplicationContext());
					localFiles = dataManager.getEntries("");
				}
				fileList.removeAllViews();
				if (localFiles.size() == 0)
				{
					TextView tv = new TextView(MainActivity.this);
					tv.setText("No files found");
					fileList.addView(tv);
				}
				else
				{
					for (FileMetaData fileMetaData : localFiles)
					{
						TextView tv = new TextView(MainActivity.this);
						tv.setText(fileMetaData.toString());
						fileList.addView(tv);
					}
				}
				fileList.invalidate();
			}
		});
        
        remoteFilesBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v)
			{
				new AsyncTask<Void, Void, List<FileMetaData>>() {
					
					List<FileMetaData> remoteFiles; 
							
					@Override
					protected List<FileMetaData> doInBackground(Void... params)
					{
						FtpFileTransferClient client = new FtpFileTransferClient("electricityservice.hostei.com", "a8926151", "ishaan007", null);
						client.connect();
						remoteFiles = client.getList();
						client.disconnect();
						return remoteFiles;
					}
					
					protected void onPostExecute(List<FileMetaData> result)
					{
						fileList.removeAllViews();
						if (remoteFiles.size() == 0)
						{
							TextView tv = new TextView(MainActivity.this);
							tv.setText("No files found on server");
							fileList.addView(tv);
						}
						else
						{
							for (FileMetaData fileMetaData : remoteFiles)
							{
								TextView tv = new TextView(MainActivity.this);
								tv.setText(fileMetaData.getFileName().toString());
								fileList.addView(tv);
							}
						}
						fileList.invalidate();
					}
				}.execute();
			}
		});
        
        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String basePath = getFilesDir() + "/bengaluru";
				fileList.removeAllViews();
				for(int i = 0; i < MySyncAdaptor.listedFolders.length; i++){
					File newFile = new File(basePath + "/" + MySyncAdaptor.listedFolders[i]);
					File[] listFiles = newFile.listFiles();
					for(int j = 0; j < listFiles.length; j++){
						TextView tv = new TextView(getApplicationContext());
						tv.setText(listFiles[j].getName());
						fileList.addView(tv);
					}
				}
				fileList.invalidate();
				/*Intent intent = new Intent(getApplicationContext(), WebPage.class);
				String url = getFilesDir() + "/bengaluru/html5/prBook1001_5.html";
				File newFile = new File(url);
				boolean ifexists = newFile.exists();
				if(!ifexists){
					Log.e(MainActivity.class.getName(), "file does not exists");
				}
				intent.putExtra("webpage", url);
				startActivity(intent);*/
			}
		});
	}
	
	/**
     * Create a new dummy account for the sync adapter
     *
     * @param context The application context
     */
    public static Account createSyncAccount(Context context)
    {
        // Create the account type and default account
        Account newAccount = new Account(ACCOUNT, ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager = (AccountManager) context.getSystemService(ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (accountManager.addAccountExplicitly(newAccount, null, null))
        {
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
        }
        else
        {
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.
             */
        }
        return newAccount;
    }
}