package com.example.sqlite;

import java.io.File;
import java.util.List;

import android.content.ContentValues;

import com.example.mysyncadapter.FileMetaData;
import com.example.mysyncadapter.MySyncService;

public class Utils {

	private DataSourceManager dataManager;
	
	public synchronized void addToDatabase(String key, FileMetaData filesToDB){
		ContentValues values = new ContentValues();
		values.put(MyDbHelper.SYNC_DATA_COL_KEY, filesToDB.getKey());
		values.put(MyDbHelper.SYNC_DATA_COL_KEY_EXT, filesToDB.getExtKey());
		values.put(MyDbHelper.SYNC_DATA_COL_NAME, filesToDB.getFileName());
		values.put(MyDbHelper.SYNC_DATA_COL_SIZE, filesToDB.getFileSize());
		values.put(MyDbHelper.SYNC_DATA_COL_TIME, filesToDB.getTimestamp());
		dataManager = new DataSourceManager();
		dataManager.enterValues(values);
		values.clear();
	}

	public void deleteFiles(List<FileMetaData> filesToBeDeleted) {
		
		for(FileMetaData file: filesToBeDeleted){
			String extPath = file.getExtKey();
			
			if(extPath.equals("")){
				String filePath = MySyncService.FILE_DIRECTORY + "/bengaluru/" + file.getKey() + "/" + file.getFileName();
				File fileToDelete = new File(filePath);
				boolean deleted = fileToDelete.delete();
				if(deleted)
					continue;
			} else {
				String filePath = MySyncService.FILE_DIRECTORY + "/bengaluru/" + file.getKey() + "/" + file.getExtKey() + "/" + file.getFileName();
				File fileToDelete = new File(filePath);
				boolean deleted = fileToDelete.delete();
				if(deleted)
					continue;
			}
				
		}
	}
}
