package com.example.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MyDbHelper extends SQLiteOpenHelper
{

    private static final String DATABASE_NAME = "synctest.db";
    private static final int DATABASE_VERSION = 1;

    // DB Table consts
    public static final String SYNC_DATA_TABLE_NAME = "file_meta_data";
    public static final String SYNC_DATA_COL_ID = "_id";
    public static final String SYNC_DATA_COL_KEY = "key_dir";
    public static final String SYNC_DATA_COL_KEY_EXT = "key_dir_ext";
    public static final String SYNC_DATA_COL_NAME = "file_name";
    public static final String SYNC_DATA_COL_SIZE = "file_size";
    public static final String SYNC_DATA_COL_TIME = "last_modified";


    // Database creation sql statement
    public static final String DATABASE_CREATE = "create table if not exists "
            + SYNC_DATA_TABLE_NAME + "(" +
            SYNC_DATA_COL_ID + " integer primary key autoincrement, " + SYNC_DATA_COL_KEY + " text not null, " +
            SYNC_DATA_COL_NAME + " text not null unique, " + SYNC_DATA_COL_SIZE + " integer not null, " + 
            SYNC_DATA_COL_TIME + " timestamp not null, " + SYNC_DATA_COL_KEY_EXT + " text" + ");";


    public MyDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MyDbHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + SYNC_DATA_TABLE_NAME);
        onCreate(db);
    }

}