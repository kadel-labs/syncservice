package com.example.sqlite;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.mysyncadapter.FileMetaData;

public class DataSourceManager {

	public static MyDbHelper helper;
	public static SQLiteDatabase database;
	
	public DataSourceManager(){
		super();
	}
	
	public DataSourceManager(Context context){
		helper = new MyDbHelper(context);
	}
	
	public synchronized boolean enterValues(ContentValues values){
		database = helper.getWritableDatabase();
		database.insertWithOnConflict(MyDbHelper.SYNC_DATA_TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
		return true;
	}
	
	public synchronized String getLastSynDate(){
		database = helper.getReadableDatabase();
		
		Cursor cursor = database.rawQuery("select * from " + MyDbHelper.SYNC_DATA_TABLE_NAME + " order by " + MyDbHelper.SYNC_DATA_COL_TIME + " desc limit 1", null);
		cursor.moveToFirst();
		String date = "";
		if(cursor.getCount() > 0){
			date = cursor.getString(4);
		}
		cursor.close();
		return date;
	}
	
	public synchronized boolean deleteEntry(int index){
		database = helper.getWritableDatabase();
		
		database.delete(MyDbHelper.SYNC_DATA_TABLE_NAME, MyDbHelper.SYNC_DATA_COL_ID + "='" + index + "'", null);
		return true;
	}
	
	public synchronized List<FileMetaData> getEntries(String key){
		if(helper == null)
			return null;
		
		database = helper.getReadableDatabase();
		
		Cursor cursor;
		if(key != ""){
			cursor = database.rawQuery("select * from " + MyDbHelper.SYNC_DATA_TABLE_NAME + " where "
				+ MyDbHelper.SYNC_DATA_COL_KEY + "=" + key, null);
		}else{
			cursor = database.rawQuery("select * from " + MyDbHelper.SYNC_DATA_TABLE_NAME, null);
		}
		
		List<FileMetaData> filesOnDisk = new ArrayList<FileMetaData>();
		cursor.moveToFirst();
		while(!cursor.isAfterLast()){
			FileMetaData files = new FileMetaData();
			files.setKey(cursor.getString(1));
			files.setFileName(cursor.getString(2));
			files.setFileSize(cursor.getLong(3));
			files.setTimestamp(cursor.getString(4));
			filesOnDisk.add(files);
			cursor.moveToNext();
		}
		cursor.close();
		return filesOnDisk;
	}
}